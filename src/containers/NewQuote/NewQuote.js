import React, {useState} from 'react';
import './NewQuote.css';
import {CATEGORIES} from "../../constants";
import axiosOrders from "../../axios-orders";

const NewQuote = props => {
  const [customer, setCustomer] = useState({
    name: '',
    text: '',
    category: '',
  });

  const customerChanged = event => {
    const name = event.target.name;
    const value = event.target.value;

    setCustomer(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const quoteHandler = async event => {
    event.preventDefault();

    const quote = {
      customer: {...customer},
    };

    try{
      await axiosOrders.post('/quotes.json', quote)
    } finally {
      props.history.replace('/');
    }
  };


  return (
      <div>
        <h2>Add new quote</h2>
        <form onSubmit={quoteHandler}>
          <select
              name="category"
              value={customer.category}
              onChange={customerChanged}
          >
            {CATEGORIES.map(item => (
                <option
                    key={item.id}
                >{item.name}</option>
            ))}
          </select>
          <h4>Author</h4>
          <input
              type="text"
              name="name"
              className="input"
              placeholder="Enter you name"
              value={customer.name}
              onChange={customerChanged}
          />
          <h4>Quote text</h4>
          <textarea
              name="text"
              className="input"
              placeholder="Enter your quote..."
              value={customer.text}
              onChange={customerChanged}
          />
          <button type="submit" className="form-btn">Save</button>
        </form>
      </div>
  );
};

export default NewQuote;