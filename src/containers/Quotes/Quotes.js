import React from 'react';
import './Quotes.css';
import {CATEGORIES} from "../../constants";
import Category from "../../components/Category/Category";
import QuoteItem from "../../components/QuoteItem/QuoteItem";

const Quotes = props => {

  return (
      <div className="main-quotes">
        <div className="categories">
          {CATEGORIES.map(num => (
              <Category
                  name={num.name}
                  key={num.id}
              />
          ))}
        </div>
        <div>
          <QuoteItem/>
        </div>
      </div>
  );
};

export default Quotes;