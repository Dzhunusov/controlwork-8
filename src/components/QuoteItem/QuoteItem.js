import React, {useEffect, useState} from 'react';
import './QuoteItem.css';
import axiosOrders from "../../axios-orders";

const BASE_URL = 'https://burgerjs7.firebaseio.com/';

const QuoteItem = props => {
  const [quotes, setQuotes] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const quoteResponse = await axiosOrders.get(BASE_URL + 'quotes.json/');
      for (let quoteResponseKey in quoteResponse.data) {
        const quoteURL = BASE_URL + 'quotes/' + quoteResponseKey + '.json';
        const quoteNameResponse = await axiosOrders.get(quoteURL);
        console.log(quoteNameResponse.data.customer);
        const newQuoteArray = quoteNameResponse.data.customer;
        //...не закончил код с GET-запросом
      }
    };
    fetchData().catch(console.error);
  });

  return quotes && (
      <div className="quote-item">
        <p>{quotes.text}</p>
        <i>{quotes.name}</i>
        <div>
          <button>Del</button>
          <button>Chng</button>
        </div>
      </div>
  );
};

export default QuoteItem;