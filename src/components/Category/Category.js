import React from 'react';
import './Category.css';

const Category = props => {
  return (
      <button
          className="category-btn"
          onClick={props.clicked}
      >{props.name}</button>
  );
};

export default Category;