export const CATEGORIES = [
  {name: 'Star Wars', id: 'star-wars'},
  {name: 'Famous people', id: 'famous-people'},
  {name: 'Saying', id: 'saying'},
  {name: 'Humour', id: 'humor'},
  {name: 'Motivational', id: 'motivational'},
]