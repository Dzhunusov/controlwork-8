import React from 'react';
import './App.css';
import {BrowserRouter, NavLink, Route, Switch} from "react-router-dom";
import Quotes from "./containers/Quotes/Quotes";
import NewQuote from "./containers/NewQuote/NewQuote";
import QuoteItem from "./components/QuoteItem/QuoteItem";

function App() {
  return (
      <BrowserRouter>
        <div className="header">
          <h1>Quotes Central</h1>
          <div className="nav-block">
            <NavLink to='/' className="nav-item" >Quotes</NavLink>
            <NavLink to='/add-quote' className="nav-item" >Submit new cuote</NavLink>
          </div>
        </div>
        <Switch>
          <Route path='/' exact component={Quotes}/>
          <Route path='/add-quote' component={NewQuote}/>
        </Switch>
      </BrowserRouter>
  );
}

export default App;
